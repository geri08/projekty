package com.company;

public class Kierownik extends Pracownik {
    private double premiaMiesieczna;

    public Kierownik(String imie, int wiek, double pensja, Dzial dzial, double premiaMiesieczna) {
        super(imie, wiek, pensja, dzial);
        this.premiaMiesieczna = premiaMiesieczna;
    }

}
