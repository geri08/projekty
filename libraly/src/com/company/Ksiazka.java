package com.company;

import java.util.List;

public class Ksiazka {
    private String tytul;
    private int liczbaStron;
    private List<Osoba> autor;
    private static int bookCounter = 0;
    private String ksiazkaId;

    public Ksiazka(String tytul, int liczbaStron, List<Osoba> autor) {
        this.tytul = tytul;
        this.liczbaStron = liczbaStron;
        this.autor = autor;
        if (bookCounter < 10) {
            ksiazkaId = "K-00" + bookCounter;
        } else if (bookCounter < 100) {
            ksiazkaId = "K-0" + bookCounter;
        } else {
            ksiazkaId = "K-" + bookCounter;
        }
    }

    public String getKsiazkaId() {
        return ksiazkaId;
    }
}
