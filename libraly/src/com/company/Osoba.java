package com.company;

public abstract class Osoba {
    private String imie;
    private int wiek;
    private static int personCounter = 0;
    private int osobaId;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
        osobaId = personCounter++;
    }

    public int getOsobaId() {
        return osobaId;
    }
}
