package com.company;

import com.company.Dzial;
import com.company.Osoba;

public class Pracownik extends Osoba {
    private double pensja;
    private Dzial dzial;

    public Pracownik(String imie, int wiek, double pensja, Dzial dzial) {
        super(imie, wiek);
        this.pensja = pensja;
        this.dzial = dzial;
    }
}
