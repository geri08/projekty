package SRC.currency;

import SRC.ScreenInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;

public class CurrencyScreen implements ScreenInterface {
    @Override
    public void start() {
        System.out.println("aktualne kursy:");

        Call<List<ListOfRates>> call = ApiService.service().getCurrency();
        call.enqueue(new Callback<List<ListOfRates>>() {
            @Override
            public void onResponse(Call<List<ListOfRates>> call, Response<List<ListOfRates>> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null)
                        showResult(response.body().get(0).getRates());
                } else {
                    System.out.println(response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<List<ListOfRates>> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private void showResult(List<Rate> rates) {
        for (Rate rate : rates) {
            System.out.printf("%s\t- %s\t- %s\n", rate.getMid(), rate.getCode(), rate.getCurrency());
        }

    }


}
