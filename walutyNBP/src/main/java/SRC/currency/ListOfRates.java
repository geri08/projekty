package SRC.currency;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListOfRates {
    @SerializedName("table")
    private String table;
    @SerializedName("no")
    private String no;
    @SerializedName("effectiveDate")
    private String date;
    @SerializedName("rates")
    private List<Rate> rates;


    public String getTable() {
        return table;
    }

    public String getNo() {
        return no;
    }

    public String getDate() {
        return date;
    }

    public List<Rate> getRates() {
        return rates;
    }
}
