package SRC.currency;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface NbpApi {
    @GET("exchangerates/tables/a/?format=json")
    Call<List<ListOfRates>> getCurrency();

    @GET("exchangerates/rates/a/{currID}/last/{amount}/?format=json")
    Call<SelectedRate> getSelectedCurrency(@Path("currID") String currencyValue,
                                           @Path("amount") int amountValue);
}
