package SRC.currency;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Rate {
    @SerializedName("currency")
    private String currency;
    @SerializedName("code")
    private String code;
    @SerializedName("mid")
    private double mid;
    @SerializedName("effectiveDate")
   private String date;
    @SerializedName("no")
    private String no;
    private List<Rate> rates;

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public double getMid() {
        return mid;
    }

    public String getDate() {
        return date;
    }

    public String getNo() {
        return no;
    }

    public List<Rate> getRates() {
        return rates;
    }
}
