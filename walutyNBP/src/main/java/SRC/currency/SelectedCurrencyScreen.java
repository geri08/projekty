package SRC.currency;

import SRC.ScreenInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.List;
import java.util.Scanner;

public class SelectedCurrencyScreen implements ScreenInterface {
    @Override
    public void start() {
        String currency = getCurrencyFromUser();
        System.out.println("kursy walut dla: " + currency.toUpperCase());
        Call<SelectedRate> call = ApiService
                .service()
                .getSelectedCurrency(currency, 10);
        call.enqueue(new Callback<SelectedRate>() {
            @Override
            public void onResponse(Call<SelectedRate> call, Response<SelectedRate> response) {
                if (response.isSuccessful()) {
                    displayResult(response.body().getRates());
                }
            }

            @Override
            public void onFailure(Call<SelectedRate> call, Throwable throwable) {
                throwable.printStackTrace();
            }
        });
    }

    private String getCurrencyFromUser() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj jaka chcesz walute sprawdzić: ");

        return scanner.nextLine();
    }

    private void displayResult(List<Rate> listRate) {
        for (Rate r : listRate) {
            System.out.printf("%s \t - %s \t \n"
            ,r.getMid(),r.getDate());
        }
    }
}
