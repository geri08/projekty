package SRC.currency;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SelectedRate {
    @SerializedName("table")
    private String table;
    @SerializedName("currency")
    private String currency;
    @SerializedName("code")
    private String code;
    @SerializedName("rates")
    private List<Rate> rates;

    public String getTable() {
        return table;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public List<Rate> getRates() {
        return rates;
    }

    @Override
    public String toString() {
        return "SelectedRate{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates=" + rates +
                '}';
    }
}
