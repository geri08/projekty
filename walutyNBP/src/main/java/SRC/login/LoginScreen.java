package SRC.login;

import SRC.ScreenInterface;
import SRC.menu.Menu;
import SRC.userlist.UserManagment;
import javafx.util.Pair;

import java.util.List;
import java.util.Scanner;

public class LoginScreen implements ScreenInterface {
    private String userName;
    private String password;
//    private char[] password;

    public void start() {
        User loggedUser;
        int triesLeft = 3;
        do {
            Pair<String, String> user = signInField();
            loggedUser = validate(user);
            if (loggedUser != null) {
                System.out.println("udało się zalogować ;)");
            } else {
                triesLeft--;
                System.out.printf("podałeś błędne dane, pozostało Ci %s prób\n", triesLeft);
            }
        } while (loggedUser == null && triesLeft > 0);

        if (loggedUser != null) {
            displayMenu(loggedUser);
        }

    }

    private void displayMenu(User loggedUser) {
        Menu menu = new Menu(loggedUser);
        menu.start();
    }

    private Pair<String, String> signInField() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj login: ");
        userName = scanner.nextLine();
        System.out.print("podaj hasło: ");
        password = scanner.nextLine();
//        scanner.close();
        return new Pair<String, String>(userName, password);
    }

    private User validate(Pair<String, String> user) {
        List<User> listOfUsers = UserManagment.getUsers();

        for (User userfromList : listOfUsers) {
            if (user.getKey().equals(userfromList.getUserName())
                    && user.getValue().equals(userfromList.getPassword())) {
                return userfromList;
            }
        }
        return null;
    }
}
