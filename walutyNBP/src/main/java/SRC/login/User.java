package SRC.login;

import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("login")
    private String userName;
    @SerializedName("password")
    private String password;
    @SerializedName("isAdmin")
    private boolean isAdmin;
    @SerializedName("name")
    private String name;
    @SerializedName("surname")
    private String surname;

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    @Override
    public String toString() {
        return "User{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
