package SRC.menu;

import SRC.ScreenInterface;
import SRC.currency.CurrencyScreen;
import SRC.currency.SelectedCurrencyScreen;
import SRC.login.User;
import SRC.userlist.ListOfUsers;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu implements ScreenInterface {
    private User user;

    public Menu(User user) {
        this.user = user;
    }

    @Override
    public void start() {
        List<MenuItems> menuItems = getMenu();
        displayMenu(menuItems);
        chooseOption(menuItems);
    }

    private void chooseOption(List<MenuItems> menuItems) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj swój wybór: ");
        int chosenOption = scanner.nextInt();
        if (chosenOption <= menuItems.size() && chosenOption > 0) {
            switch (menuItems.get(chosenOption - 1)) {
                case MENU_LASTDAY:
                    System.out.printf("wybrana opcja: kurs z ostatniego dnia");
                    new CurrencyScreen().start();
                    break;
                case MENU_CURRENCY:
                    System.out.println("wybrana opcka - wybrana waluta z ostatnich dni:");
                    new SelectedCurrencyScreen().start();
                    break;
                case MENU_USERLIST:
                    new ListOfUsers().start();
                    break;
                case MENU_SELECTDAY:
                    break;
            }
        } else {
            chooseOption(menuItems);
        }
    }

    private void displayMenu(List<MenuItems> menuItems) {
        System.out.println("wybierz co chcesz zrobić: ");
        for (int i = 0; i < menuItems.size(); i++) {
            System.out.printf("%s\t%s\n", i + 1, menuItems.get(i).getMessage());
        }
    }

    private List<MenuItems> getMenu() {
        List<MenuItems> menuItems = new ArrayList<MenuItems>();
        menuItems.add(MenuItems.MENU_CURRENCY);
        menuItems.add(MenuItems.MENU_LASTDAY);
        menuItems.add(MenuItems.MENU_SELECTDAY);

        if (user.isAdmin()) {
            menuItems.add(0, MenuItems.MENU_USERLIST);
        }

        return menuItems;
    }
}
