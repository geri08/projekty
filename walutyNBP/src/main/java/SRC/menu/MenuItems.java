package SRC.menu;

public enum MenuItems {
    MENU_USERLIST("Wyświetl liste użytkowników"),
    MENU_LASTDAY("Pobierz dane z ostatniego dnia"),
    MENU_SELECTDAY("Pobierz dane z wybranego dnia"),
    MENU_CURRENCY("Pobierz dane wybranej waluty");

    private String message;

    MenuItems(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
