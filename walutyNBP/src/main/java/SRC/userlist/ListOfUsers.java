package SRC.userlist;

import SRC.ScreenInterface;
import SRC.login.User;

import java.util.List;

public class ListOfUsers implements ScreenInterface {
    @Override
    public void start() {
        getUsersFromJson();
    }

    private void getUsersFromJson() {
        List<User> userList = UserManagment.getUsers();
        System.out.printf("lista uzytkowników: (%s)\n", userList.size());
        userList.stream()
                .map(e -> String.format("imie: %s ,\t nazwisko: %s", e.getUserName(), e.getSurname()))
                .forEach(e -> System.out.println(e));

    }

}
