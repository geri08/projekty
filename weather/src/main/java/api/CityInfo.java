package api;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.ToString;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@ToString

public class CityInfo {
    @SerializedName("title")
    private String cityname;
    @SerializedName("location_type")
    private String locationType;
    @SerializedName("latt_long")
    private String lattLong;
    @SerializedName("sun_rise")
    private String sunRise;
    @SerializedName("sun_set")
    private String sunSet;
    @SerializedName("parent")
    private City parent;
    @SerializedName("consolidated_weather")
    private List<ConsolidatedWeather> weatherlist;

    public void displayInfo() {
        System.out.println("city: " + cityname);
        System.out.println("country: " + parent.getTitle());
        System.out.printf("min temp: %.2f°C\n", getAverageTemp(true));
        System.out.printf("max temp: %.2f°C\n", getAverageTemp(false));
        System.out.println("sunrise: " + getSunriseSunset(sunRise));
        System.out.println("sunset: " + getSunriseSunset(sunSet));
        System.out.println("google Maps link: " + getGoogleMapsLink());
        createHtml();
    }

    private Map<String, String> generateMap() {
        Map<String, String> map = new HashMap<>();
        map.put("City", cityname);
        map.put("locationType", parent.getTitle());
        map.put("min temp:", getAverageTemp(true) + "°C");
        map.put("max temp:", getAverageTemp(false) + "°C");
        map.put("sunrise: ", getSunriseSunset(sunRise));
        map.put("sunset: ", getSunriseSunset(sunSet));
        map.put("google Maps link: ","<a href=\""+ getGoogleMapsLink()+ "\">Google Maps</a>");
        return map;
    }

    public String prepareContent() {
        Map<String, String> map = generateMap();
        StringBuilder sb = new StringBuilder();
        sb.append("<TABLE>");

        for (Map.Entry<String, String> entry : map.entrySet()) {
            sb.append("<TR>");
            sb.append("<TD>" + entry.getKey() + "</TD>");
            sb.append("<TD>" + entry.getValue() + "</TD>");
            sb.append("</TR>");
        }
        sb.append("</TABLE>");
        return sb.toString();
    }

    public void createHtml() {
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = new FileOutputStream("weather.html");
            fileOutputStream.write(prepareContent().getBytes());
            fileOutputStream.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    private String getGoogleMapsLink() {
        https:
//www.google.pl/maps/@51.7838899,19.4480326,15z
        return String.format("https://www.google.pl/maps/@%s,15z", lattLong);
    }

    private String getSunriseSunset(String sun) {
        DateTime dateFormat = ISODateTimeFormat.
                dateTime().
                parseDateTime(sun);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm, dd.MM.yyyy");
        return formatter.print(dateFormat);
    }

    private double getAverageTemp(boolean isMin) {
//        double sum = 0;
//        for (ConsolidatedWeather weather : weatherlist) {
//            if (isMin) {
//                averageTemp += weather.getMinTemp();
//            } else {
//                averageTemp += weather.getMaxTemp();
//            }
//        }

        double sum = weatherlist.stream()
                .map(s -> isMin ? s.getMinTemp() : s.getMaxTemp())
                .mapToDouble(s -> s.doubleValue())
                .sum();
        return sum / weatherlist.size();
    }
}
