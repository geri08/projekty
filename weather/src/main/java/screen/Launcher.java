package screen;

import api.ApiConnector;
import api.City;
import api.CityInfo;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Launcher {
    public static void main(String[] args) {
        List<City> listOfCities;
        String cityName;
        do {
            cityName = getCityNameFromUser();
            listOfCities = getListOfCities(cityName);
        } while (listOfCities.isEmpty());
        displayListOfCities(listOfCities);
        int chosenIndexFromList = getIndexFromUser(listOfCities.size());
        CityInfo cityInfo = getCityInfo(listOfCities.get(chosenIndexFromList - 1).getId());
        cityInfo.displayInfo();

    }


    private static CityInfo getCityInfo(int chosenIndexFromList) {
        try {
            Response<CityInfo> response = ApiConnector.getCallOfCities(chosenIndexFromList).execute();
            if (response != null && response.body() != null) {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static int getIndexFromUser(int sizeOfList) {
        Scanner scanner = new Scanner(System.in);
        int chosenOption;
        do {
            System.out.print("Choose option from the given list: ");
            chosenOption = scanner.nextInt();
        } while (chosenOption <= 0 || chosenOption > sizeOfList);
        return chosenOption;

    }

    private static String getCityNameFromUser() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("podaj nazwe miasta: ");
        return scanner.nextLine();
    }

    private static List<City> getListOfCities(String cityName) {
//        ApiConnector.getCallOfCities(cityName).enqueue(getCallback());

        List<City> list = new ArrayList<>();

        try {
            Response<List<City>> response = ApiConnector.getCallOfCities(cityName).execute();
            if (response != null && response.body() != null) {
                list = response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return list;
    }

    private static void displayListOfCities(List<City> cities) {
        for (int i = 0; i < cities.size(); i++) {
            System.out.printf("%s. %s, woeid: %s\n",
                    (i + 1),
                    cities.get(i).getTitle(),
                    cities.get(i).getId());
        }

    }
    //        };
    //            }
    //
    //                throwable.printStackTrace();
    //            public void onFailure(Call<List<City>> call, Throwable throwable) {
    //
    //            }
    //                }
    //                    displayListOfCities(response.body());
    //                if (response.isSuccessful() && response.body() != null) {
    //            public void onResponse(Call<List<City>> call, Response<List<City>> response) {
    //        return new Callback<List<City>>() {
//    private static Callback<List<City>> getCallback() {

//    }
}
