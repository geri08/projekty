package Zadanie1;

public class Bird extends Pet {
    public Bird(String name, double weight, int age, String owner) {
        super(name, weight, age, owner);
    }

    @Override
    public String introduceYourself() {
        return super.introduceYourself() + " jestem ptakiem";
    }


    @Override
    public String toString() {
        return super.toString();
    }

    public void swingWhenFlying() {
        System.out.println("ptak śpiewa gdy lata");
    }
}
