package Zadanie1;

public class Cat extends Pet implements OwnerHolder {
    public Cat(String name, double weight, int age, String owner) {
        super(name, weight, age, owner);
    }

    @Override
    public String introduceYourself() {
        return super.introduceYourself() + " jestem kotem";
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void drinkMilk() {
        System.out.println("kot pije mleko");
    }

}
