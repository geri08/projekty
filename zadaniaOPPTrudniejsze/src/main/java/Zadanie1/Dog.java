package Zadanie1;

public class Dog extends Pet {
    public Dog(String name, double weight, int age, String owner) {
        super(name, weight, age, owner);
    }

    @Override
    public String introduceYourself() {
        return super.introduceYourself() + " jestem psem";
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void biteBone() {
        System.out.println("pies gryzie kość");
    }
}
