package Zadanie1;

public class Human extends Mammal {

    public Human(String name, double weight, int age) {
        super(name, weight, age);
    }

    @Override
    public String introduceYourself() {
        return super.introduceYourself() + " jestem człowiekiem";
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void drinkWater() {
        System.out.println("człowiek pije wode");
    }
}
