package Zadanie1;
/*
1.
Stwórzcie abstrakcyjną klasa Animal z polami:
-name
-weight
-age

z abstrakcyjna metodą wypisującą przedstawianie się
-String introduceYourself()
oraz konstruktorem dla wszystkich pól

Następnie utwórzcie klasę Pet extendującą Animal zawierającą pole
-owner typu Person
z konstruktorem odnoszącym się w pierwszej linii do nadklasy (słowo super) a potem do pola owner


Klasa Dog dziedzicząca z Pet + implementacja metody introduceYourself() - np "jestem psem"
Klasa Cat dziedzicząca z Pet + implementacja metody introduceYourself()
Klasa Bird dziedzicząca z Pet + implementacja metody introduceYourself()

Stwórzcie odpowiednią metodę toString dla każdej z klas aby wyświetlały się dane dla każdej z nich

Dodatkowo utwórzcie abstrakcyjną klasę Mammal (ssak), która będzie dziedziczyć z Animal i następnie
klasę Human jako extendującą Mammal
+ implementacja metody introduceYourself()
+ konstruktor

 */


public class Main {
    public static void main(String[] args) {

//        zadanie2();
//        zadanie3();
//        zadanie4();
        zadanie5();
    }


    /*
    2.
    W osobnej metodzie
    Utwórzcie obiekty
    Dog dog,Cat cat,Bird bird,Human human
    wraz z danymi
    Wypiszcie wartości przekazywane przez toString (wystarczy przekazać obiekt)
     */

    private static void zadanie2() {
        Dog dog = new Dog("azor", 15, 10, "adam");
        Cat cat = new Cat("hela", 5, 3, "michal");
        Bird bird = new Bird("elemelek", 0.5, 1, "zuzia");
        Human human = new Human("zosia", 55.4, 22);
        System.out.println(dog);
        System.out.println(cat);
        System.out.println(bird);
        System.out.println(human);
    }

    /*
    Następnie w osobnej metodzie
Utwórzcie obiekty
Animal dog,Animal cat,Animal bird,Animal human
wraz z danymi i załadujcie je do tablicy, następnie w pętli wypiszcie tekst przy pomocy którego przedstawiają się
     */
    private static void zadanie3() {
        Animal dog = new Dog("azor", 15, 10, "adam");
        Animal cat = new Cat("hela", 5, 3, "adam");
        Animal bird = new Bird("elemelek", 0.5, 1, "adam");
        Animal human = new Human("zosia", 55.4, 22);
        Animal[] tablica = new Animal[]{dog, cat, bird, human};
        for (Animal animal : tablica) {
            System.out.println(animal.introduceYourself());
        }
    }

    /*
    4.
    Dodajcie do klas:
    Cat - metodę void drinkMilk() wypisującą, że kot pije mleko
    Dog - metodę void biteBone() wypisującą, że gryzie kość
    Bird - metodę void singWhenFlying() wypisującą, że ptak śpiewa podczas latania

    Następnie w osobnej metodzie

    Utwórzcie obiekty
    Animal dog,cat,bird,human
    wraz z danymi i załadujcie je do tablicy, następnie w pętli użyjcie metody dodane w poprzednim
    punkcie (drinkMilk(),biteBone(),singWhenFlying()) przez wykorzystanie instanceof oraz
    rzutowania na bardziej szczegółowe typy
     */

    private static void zadanie4() {
        Animal dog = new Dog("azor", 15, 10, "adam");
        Animal cat = new Cat("hela", 5, 3, "adam");
        Animal bird = new Bird("elemelek", 0.5, 1, "adam");
        Animal human = new Human("zosia", 55.4, 22);
        Animal[] tablica = new Animal[]{dog, cat, bird, human};
        for (Animal animal : tablica) {
            if (animal instanceof Dog) {
                Dog d = (Dog) animal;
                d.biteBone();
            } else if (animal instanceof Cat) {
                Cat c = (Cat) animal;
                c.drinkMilk();
            } else if (animal instanceof Bird) {
                Bird b = (Bird) animal;
                b.swingWhenFlying();
            } else if (animal instanceof Human) {
                Human h = (Human) animal;
                h.drinkWater();
            }
        }
    }

    /*
    5
    Zarówno Klasa Pet jak i Car mają pola przechowujące właściciela (owner), można zatem stworzyć interfejs dla tych klas.
    Stwórzcie interfejs OwnerHolder i dodajcie tam metodę
    -String getOwnerName()

    Następnie w klasach Pet i Car dodajcie implementację tej metody przez wyciągnięcie imienia z pola owner.

    6.
    W osobnej metodzie:
    Utwórzcie obiekty
    OwnerHolder car
    OwnerHolder dog
    wraz z podstawowymi danymi i następnie wypiszcie imie wlasciciela
     */
    private static void zadanie5() {
        OwnerHolder dog = new Dog("azor", 15, 10, "adam");
        OwnerHolder cat = new Cat("hela", 5, 3, "ania");
        System.out.println(dog.getOwnerName());
        System.out.println(cat.getOwnerName());
    }
}
