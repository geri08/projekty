package Zadanie1;

public abstract class Mammal extends Animal {
    public Mammal(String name, double weight, int age) {
        super(name, weight, age);
    }

    public String introduceYourself() {
        return String.format("hej, nazywam się %s, ważę: %s kg i mam %s lat", Mammal.this.getName(),
                Mammal.super.getAge(), Mammal.this.getAge());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
