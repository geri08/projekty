package Zadanie1;

public class Pet extends Animal implements OwnerHolder {
    private String owner;


    public Pet(String name, double weight, int age, String owner) {
        super(name, weight, age);
        this.owner = owner;
    }

    public String introduceYourself() {
        return String.format("hej, nazywam się %s, ważę: %s kg i mam %s lat", Pet.this.getName(), Pet.super.getAge(), Pet.this.getAge());
    }

    @Override
    public String toString() {
        return super.toString() + " owner: " + owner;
    }

    public String getOwnerName() {
        return this.owner;
    }
}
